let num1 = prompt("Enter first number");
let num2 = prompt("Enter second number");

let operation = prompt("Enter operation");
while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
  operation = prompt("Enter correct operation!");
}

function mathOp(operation) {
  switch (operation) {
    case '+':
      return (+num1 + +num2);
    case '-':
      return (num1 - num2);
    case '*':
      return (num1 * num2);
    case '/':
      return (num1 / num2);
  }
}

console.log(mathOp(operation));